package com.daniel.eurekadiscovery

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class EurekadiscoveryApplication {

	static void main(String[] args) {
		SpringApplication.run EurekadiscoveryApplication, args
	}
}
